/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import models.Product;
import views.MainForm;



public class ProductController implements ActionListener {
    MainForm mainForm;
    JFileChooser d;
    Product product;

    public ProductController(MainForm f) {
        super();
        mainForm = f;
        d = new JFileChooser();
        product = new Product();
    }
    
    public void setProduct (Product p){
        product = p;
    }
    
    public void setProduct(String filePath){
        File f = new File(filePath);
        readProduct(f);
    }
    public Product getProduct(){
        return product;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "Select":
                d.showOpenDialog(mainForm);  
                product = readProduct(d.getSelectedFile());
                mainForm.setProductData(product);
                break;
            case "clear":
                mainForm.clear();
                break;
            case "save":
                d.showSaveDialog(mainForm); 
                product = mainForm.getProductData();
                writeProduct(d.getSelectedFile());
                break;
        }
    }

    private Product readProduct(File file) throws IOException {
        try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Product)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(mainForm, e.getMessage(),mainForm.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void writeProduct(File file) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getProduct());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
   
}
